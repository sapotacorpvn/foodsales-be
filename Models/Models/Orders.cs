using System.ComponentModel.DataAnnotations;

namespace Models.Models
{
    public class Orders
    {
        [Key]
        public int Id { get; set; }
        public string? Region { get; set; }
        public string? City { get; set; }
        public DateTime CreateOn { get; set; } = DateTime.Now;
        public DateTime? UpdateOn { get; set; }
        public string? Category { get; set; }
        public string? Product { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice
        {
            get { return Quantity * UnitPrice; }
        }
    }
}