﻿using Models.Models;

namespace Models.Dto
{
    public class OrderDto
    {
        public string? Region { get; set; }
        public string? City { get; set; }
        public string? Category { get; set; }
        public string? Product { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
    }
    public class CreateOrderReponse
    {

        public int Id { get; set; }
        public string? Region { get; set; }
        public string? City { get; set; }
        public string? Category { get; set; }
        public string? Product { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public DateTime? CreateOn { get; set; }
    }

    public class GetOrders
    {
        public int? Id { get; set; }
        public string? SearchTerm{ get; set; }
        public int Pages { get; set; }
        public int ItemPerPage { get; set; }
        public string? SortByField { get; set; }
        public bool? SortASC { get; set; }
        public DateTime? OrderedFrom { get; set; }
        public DateTime? OrderedTo { get; set; }
    }

    public class OrderListResponse
    {
        public int? Id { get; set; }
        public string? Region { get; set; }
        public string? City { get; set; }
        public string? Category { get; set; }
        public string? Product { get; set; }
        public int? Quantity { get; set; }
        public double? UnitPrice { get; set; }
        public double? TotalPrice { get; set; }
        public DateTime? CreateOn { get; set; }
        public DateTime? UpdateOn { get; set; }

    }

    public class OrderResponse
    {
        public List<Orders> Orders { get; set; } = new List<Orders>();
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public int TotalItem { get; set; }
    }
}
