using AutoMapper;
using Helpers.Mapper;
using Microsoft.EntityFrameworkCore;
using Models.Models;
using Common.Constant;
using Services.Interfaces;
using Services.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<FFDbContext>(
    options => options.UseSqlServer(AppConfig.DefaultConnection)
);

var mapperConfig = new MapperConfiguration(
    
    mc => { mc.AddProfile(new MapperProfile()); }
    
);


IMapper mapper = mapperConfig.CreateMapper();
builder.Services.AddSingleton(mapper);
builder.Services.AddTransient<IOrder, OrderSevices>();
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSwaggerGen(options =>
{
    //options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
    //{
    //    Title = "learn-dotnet API",
    //    Version = "v1",
    //});
    options.AddSecurityDefinition("bearerAuth", new OpenApiSecurityScheme
    {
        Description = "JWT containing userid claim",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.Http,
        Scheme = "bearer",
        BearerFormat = "JWT"
    });

    var security =
        new OpenApiSecurityRequirement
        {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Id = "bearerAuth",
                                    Type = ReferenceType.SecurityScheme
                                },
                                UnresolvedReference = true
                            },
                            new List<string>()
                        }
        };
    options.AddSecurityRequirement(security);
});


builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = AppConfig.Jwt.Issuer,
            ValidAudience = AppConfig.Jwt.Issuer,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppConfig.Jwt.Secret))
        };
    }
    );

// TODO: In the demo, we will allow access API from everywhere. But in real project, we should restrict it.
builder.Services.AddCors(
    cors =>
        cors.AddPolicy(
            "AllowAnyOrigin",
            builder =>
            {
                builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
            }
        )
);

var app = builder.Build();

app.UseCors("AllowAnyOrigin");
app.UseCors(options => options.AllowAnyOrigin());

app.UseSwagger();    
app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "learn-donet API"));
app.UseHttpsRedirection();
app.UseAuthorization();
app.UseAuthentication();
app.MapControllers();

app.Run();
