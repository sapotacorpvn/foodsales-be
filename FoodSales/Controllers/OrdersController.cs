﻿using Microsoft.AspNetCore.Mvc;
using Models.Dto;
using Services.Interfaces;

namespace FoodSales.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrder _orderSevices;

        public OrdersController(IOrder orderSevices)
        {
            _orderSevices = orderSevices;
        }

        [HttpGet]
        public async Task<ObjectResult> Get([FromQuery] GetOrders getOrders)
        {
            try
            {
                var response = await _orderSevices.GetOrders(getOrders);
                return StatusCode(StatusCodes.Status200OK, response);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPost]
        public async Task<ObjectResult> Create(OrderDto order)
        {
            try
            {
                var response = await _orderSevices.CreateOrder(order);
                return StatusCode(StatusCodes.Status201Created, response);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPatch]
        public async Task<ObjectResult> Update([FromQuery] int id, OrderDto order)
        {
            try
            {
                var response = await _orderSevices.UpdateOrders(id, order);
                if (response)
                {
                    return StatusCode(StatusCodes.Status200OK, "Food sales updated Successfully");
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, "Food sales is not existed");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpDelete]
        public async Task<ObjectResult> Delete(int? id)
        {
            try
            {
                var response = await _orderSevices.DeteleOrders(id);
                return StatusCode(StatusCodes.Status204NoContent, "Food sales deleted Successfully");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
