﻿using AutoMapper;
using Models.Models;
using Models.Dto;

namespace Helpers.Mapper
{
    public class MapperProfile: Profile
    {
        public MapperProfile()
        {
            CreateMap<Orders, OrderListResponse>();
        }
    }
}
