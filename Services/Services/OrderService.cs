﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Models.Dto;
using Models.Models;
using Services.Interfaces;

namespace Services.Services
{
    public class OrderSevices : IOrder
    {
        private readonly FFDbContext _contextService;

        public OrderSevices(FFDbContext contextService)
        {
            _contextService = contextService;
        }

        public async Task<OrderResponse> GetOrders(GetOrders getOrders)
        {
            var query = _contextService.Orders.AsQueryable();
            if (getOrders.Id > 0)
                query = query.Where(i => i.Id == getOrders.Id);
            if (getOrders.SearchTerm != null)
                query = query.Where(i => (i.Region != null && i.Region.Contains(getOrders.SearchTerm)) || (i.City != null && i.City.Contains(getOrders.SearchTerm)) || (i.Category != null && i.Category.Contains(getOrders.SearchTerm)) || (i.Product != null && i.Product.Contains(getOrders.SearchTerm)));
            if (getOrders.OrderedFrom.HasValue)
                query = query.Where(i => i.CreateOn.Date >= getOrders.OrderedFrom);
            if (getOrders.OrderedTo.HasValue)
                query = query.Where(i => i.CreateOn.Date <= getOrders.OrderedTo);

            if (getOrders.Pages == 0)
            {
                getOrders.Pages = 1;
            }
            if (getOrders.ItemPerPage == 0 || getOrders.ItemPerPage > 100)
            {
                getOrders.ItemPerPage = 100;
            }
            var totalItem = query.Count();
            if (!string.IsNullOrEmpty(getOrders.SortByField))
            {
                var sortFieldLowerCase = getOrders.SortByField.ToLower();
                switch (sortFieldLowerCase)
                {
                    case "region":
                        query = getOrders.SortASC == true ? query.OrderBy(o => o.Region) : query.OrderByDescending(o => o.Region);
                        break;
                    case "city":
                        query = getOrders.SortASC == true ? query.OrderBy(o => o.City) : query.OrderByDescending(o => o.City);
                        break;
                    case "category":
                        query = getOrders.SortASC == true ? query.OrderBy(o => o.Category) : query.OrderByDescending(o => o.Category);
                        break;
                    case "product":
                        query = getOrders.SortASC == true ? query.OrderBy(o => o.Product) : query.OrderByDescending(o => o.Product);
                        break;
                    case "quantity":
                        query = getOrders.SortASC == true ? query.OrderBy(o => o.Quantity) : query.OrderByDescending(o => o.Quantity);
                        break;
                    case "unitprice":
                        query = getOrders.SortASC == true ? query.OrderBy(o => o.UnitPrice) : query.OrderByDescending(o => o.UnitPrice);
                        break;
                    case "totalprice":
                        query = getOrders.SortASC == true ? query.OrderBy(o => o.UnitPrice * o.Quantity) : query.OrderByDescending(o => o.UnitPrice * o.Quantity);
                        break;
                    default:
                        query = getOrders.SortASC == true ? query.OrderBy(o => o.CreateOn) : query.OrderByDescending(o => o.CreateOn);
                        break;
                }
            }
            List<Orders> orderList = await query.Skip((int)((getOrders.Pages - 1) * getOrders.ItemPerPage))
            .Take(getOrders.ItemPerPage)
            .ToListAsync();
            var totalPages = (int)Math.Ceiling((decimal)((totalItem + getOrders.ItemPerPage - 1) / getOrders.ItemPerPage));
            return new OrderResponse {
                Orders = orderList,
                CurrentPage = getOrders.Pages,
                TotalItem = totalItem,
                TotalPages = totalPages
            } ;
        }

        public async Task<CreateOrderReponse> CreateOrder(OrderDto order)
        {
            var newOrder = new Orders()
            {
                Region = order.Region,
                City = order.City,
                Category = order.Category,
                Product = order.Product,
                Quantity = order.Quantity,
                UnitPrice = order.UnitPrice,
                CreateOn = DateTime.Now
            };

            _contextService.Orders.Add(newOrder);
            await _contextService.SaveChangesAsync();
            return new CreateOrderReponse()
            {
                Id = newOrder.Id,
                Region = newOrder.Region,
                City = newOrder.City,
                Category = newOrder.Category,
                Product = newOrder.Product,
                Quantity = newOrder.Quantity,
                UnitPrice = newOrder.UnitPrice,
                TotalPrice = newOrder.TotalPrice,
                CreateOn = newOrder.CreateOn
            };

        }

        public async Task<bool> UpdateOrders(int id, OrderDto order)
        {
            var updatedOrder = await _contextService.Orders.Where(o => o.Id == id).FirstOrDefaultAsync();
            if (updatedOrder != null)
            {
                updatedOrder.Region = order.Region;
                updatedOrder.City = order.City;
                updatedOrder.Category = order.Category;
                updatedOrder.Product = order.Product;
                updatedOrder.Quantity = order.Quantity;
                updatedOrder.UnitPrice = order.UnitPrice;
                updatedOrder.UpdateOn = DateTime.Now;
                await _contextService.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> DeteleOrders(int? id)
        {
            var deletedOrder = await _contextService.Orders.Where(b => b.Id == id).FirstOrDefaultAsync();
            if (deletedOrder != null)
            {
                _contextService.Orders.Remove(deletedOrder);
                await _contextService.SaveChangesAsync();

            }
            return false;
        }
    }
}
