﻿using Models.Dto;
using Models.Models;

namespace Services.Interfaces
{
    public interface IOrder
    {
        Task<CreateOrderReponse> CreateOrder(OrderDto order);
        Task<bool> UpdateOrders(int id, OrderDto order);
        Task<bool> DeteleOrders(int? id);
        Task<OrderResponse> GetOrders(GetOrders getOrders);
    }
}
